import Http from './http';
import Render from './render';

export default class Searcher {

    constructor(input_selector, url_base) {

        this.search = this.search.bind(this);
        this.input = document.querySelector(input_selector);
        this.url = url_base;
        this.api_key = 'dzT61WT7icV6BPib3BI4p79KkvG7Wyu3'; // this should not go here
        this.value = 'random'; //search by default
        this.offset = 0;
        this.interval = null;

        this.bindsEvents();
    }

    // search request
    search() {
        Http.get(`${this.url}/search?q=${this.value}&api_key=${this.api_key}&limit=50&offset=${this.offset}`)
            .then(res => {
                this.offset = res.pagination.offset + 50;
                let render = new Render();
                render.showPreview(res);
            }).catch(err => { console.log(err) });
    }

    // event to write client input text
    bindsEvents() {
        this.input.addEventListener('change', () => {
            if (this.input.value == this.value || this.input.value.length < 3) return;
            if (this.interval) window.clearInterval(this.interval);
            this.value = this.input.value;
            this.interval = window.setTimeout(() => {
                document.querySelector('#images').innerHTML = '';
                this.search();
            }, 500);
        });

        // capture the scroll event to bring more data
        const container = document.documentElement;
        document.addEventListener('scroll', () => {
            if (container.scrollTop + container.clientHeight >= container.scrollHeight) {
                this.search();
            }
        });
    }
}