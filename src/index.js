import Searcher from './searcher';

const API_URL = 'http://api.giphy.com/v1/gifs'; // url api GIPHY

let searcher = new Searcher('#txtsearch', API_URL);
searcher.search();