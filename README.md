# Challenge for Bluecoding

This project allows you to search for animated GIFs using the GIPHY API

## Requirements

To start this project on your machine you need to have installed `Node`

## Start project

In your terminal execute the following commands:

```bash
$ npm install
$ npm run start
```

Webpack will raise a local server and you can see the site at [http://localhost:8000](http://localhost:8000)


## Build

Run `npm run build` to build the project. This will create a `bundle.js` file in the `build` directory

## Author

Elkin Urango - [@kin3xc](https://twitter.com/kin3xc)
