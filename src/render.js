export default class Render {

    constructor() {
        this.imageIndex = 0;
        this.images = [];
        this.bindsEvents();
    }

    // Initialization of events in the app
    bindsEvents() {
        const btnClose = document.querySelector('.close');
        const btnNext = document.querySelector('.next');
        const btnBack = document.querySelector('.back');
        const body = document.querySelector('body');

        // to close the modal
        btnClose.addEventListener('click', () => {
            this.close();
        });

        // event to advance to the next image
        btnNext.addEventListener('click', () => {
            this.next();
        });

        // event to go to the previous image
        btnBack.addEventListener('click', () => {
            this.back();
        });

        // capture the events of the right and left keys
        body.addEventListener('keydown', (e) => {
            if (e.code === 'ArrowLeft') this.back();
            if (e.code === 'ArrowRight') this.next();
            if (e.code === 'Escape') this.close();
        })
    }

    // show gif animate in modal
    loadModal(item, index, images) {
        this.imageIndex = index;
        this.images = images;
        this.showGif(item.images.original.url);
    }

    // close modal
    close() {
        let modal = document.querySelector('.modal')
        modal.classList.remove("visible");
        modal.querySelector(".panel .content").innerHTML = '';
    }

    // Show the GIF in the previously loaded modal
    showGif(image) {
        let modal = document.querySelector('.modal');
        let panel = modal.querySelector(".panel");

        let content = modal.querySelector(".content");
        let img = document.createElement('img');
        content.appendChild(img);
        img.setAttribute('src', image);

        panel.style.maxWidth = '100%';
        panel.style.maxHeight = '100%';

        modal.classList.add("visible");
    }

    // advance a position in the modal
    next() {
        if (this.imageIndex < this.images.length - 1) {
            modal.querySelector(".panel .content").innerHTML = '';
            this.showGif(this.images[this.imageIndex + 1].images.original.url);
            this.imageIndex++;
        }
    }

    // retreats a position in the modal
    back() {
        if (this.imageIndex > 0) {
            modal.querySelector(".panel .content").innerHTML = '';
            this.showGif(this.images[this.imageIndex - 1].images.original.url);
            this.imageIndex--;
        }
    }

    // show preview images in web site
    showPreview(result) {
        let images = document.querySelector('#images');

        result.data.forEach((item, index) => {
            let img = document.createElement('img');
            images.appendChild(img)
            img.setAttribute('src', item.images.fixed_width_small_still.url);
            img.setAttribute('alt', item.title);
            img.setAttribute('title', item.title);
            img.addEventListener('click', () => { this.loadModal(item, index, result.data) });
        })
    }
}